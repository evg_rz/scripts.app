<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([ 'prefix' => 'datetime' ], function () {

    Route::get('get-timezone-list', 'DateTimeController@getTimezoneList');
    Route::get('get-timezone-by-name', 'DateTimeController@getTimezoneByName');
    Route::get('get-timezone-by-offset', 'DateTimeController@getTimezoneByOffset');

    Route::get('get-datetime-with-timezone', 'DateTimeController@getDateTimeWithTimezone');
    Route::get('get-current-datetime-with-timezone', 'DateTimeController@getCurrentDateTimeWithTimezone');

});

Route::group([ 'prefix' => 'string_algorithms' ], function () {

});

Route::group([ 'prefix' => 'number_algorithms' ], function () {

});

Route::group([ 'prefix' => 'patterns' ], function () {

});