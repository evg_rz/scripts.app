<?php
/**
 * Created by PhpStorm.
 * User: rz
 * Date: 03.07.17
 * Time: 20:19
 */
namespace App\Base;

class Result {
    /**
     * Коды ответа
     */
    const
        OK = 200,
        INTERNAL_SERVER_ERROR = 500,
        INVALID_REQUEST_PARAMETERS = 600
    ;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var bool
     */
    private $valid = false;

    /**
     * @var array
     */
    private $messages = [];

    /**
     * @var array
     */
    private $errorMessages = [];

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return (boolean)$this->valid;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setIsValid($value)
    {
        $this->valid = (boolean)$value;
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setMessage($value)
    {
        $this->messages = $value;
        return $this;
    }

    /**
     * @param $value
     * @param bool $keyField
     * @param bool $isArray
     * @return $this
     */
    public function addMessage($value, $keyField = false, $isArray = false)
    {
        $this->setIsValid(false);

        if ($keyField) {
            if ($isArray) {
                $this->messages[$keyField][] = $value;
            } else {
                $this->messages[$keyField] = $value;
            }
        } else {
            $this->messages[] = $value;
        }

        return $this;
    }

    /**
     * @param $value
     * @param bool $keyField
     * @param bool $isArray
     * @return $this
     */
    public function addSuccessMessage($value, $keyField = false, $isArray = false)
    {
        $this->setIsValid(true);

        if ($keyField) {
            if ($isArray) {
                $this->messages[$keyField][] = $value;
            } else {
                $this->messages[$keyField] = $value;
            }
        } else {
            $this->messages[] = $value;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearMessages()
    {
        $this->messages = [];
        return $this;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    /**
     * @param $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage)
    {
        $this->setIsValid(false);

        $this->errorMessages = $errorMessage;
        return $this;
    }

    /**
     * @param $value
     * @param bool $keyField
     * @param bool $isArray
     * @return $this
     */
    public function addErrorMessage($value, $keyField = false, $isArray = false)
    {
        $this->setIsValid(false);

        if ($keyField) {
            if ($isArray) {
                $this->errorMessages[$keyField][] = $value;
            } else {
                $this->errorMessages[$keyField] = $value;
            }
        } else {
            $this->errorMessages[] = $value;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearErrorMessages()
    {
        $this->errorMessages = [];
        return $this;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $result = [
            'status' => $this->isValid() ? self::OK : $this->getCode(),
            'error_messages' => $this->getErrorMessages(),
            'data' => $this->getMessages(),
        ];

        return $result;
    }

    /**
     * @param $array
     * @return $this
     */
    public function fromArray($array)
    {
        return $this
            ->setIsValid(isset($array['is_valid']) ? $array['is_valid'] : false)
            ->setCode(isset($array['status']) ? $array['status'] : null)
            ->setErrorMessage(isset($array['error_messages']) ? $array['error_messages'] : [])
            ->setMessage(isset($array['message']) ? $array['message'] : [])
        ;
    }

    /**
     * @param $value
     * @return $this
     */
    public function fromString($value)
    {
        return $this->fromArray(json_decode($value, true));
    }

}