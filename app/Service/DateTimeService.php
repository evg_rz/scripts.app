<?php
/**
 * Created by PhpStorm.
 * User: rz
 * Date: 03.07.17
 * Time: 20:21
 */
namespace App\Service;

use Carbon\Carbon;

class DateTimeService
{
    /**
     * Получить все временные зоны
     *
     * @return array
     */
    public static function getTimezones()
    {
        $result = [];
        $timezoneList = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);

        foreach ($timezoneList as $timezone) {
            $timezoneInfo = new \DateTimeZone($timezone);
            $offset = $timezoneInfo->getOffset(new \DateTime);
            $result[$timezoneInfo->getName()] = [
                'name' => $timezoneInfo->getName(),
                'offset_prefix' => $offset < 0 ? '-' : '+',
                'offset' => gmdate('H:i', abs($offset))
            ];
        }

        return $result;
    }

    /**
     * Получить временную зону по имени
     *
     * @param $name
     * @return mixed
     */
    public static function getTimezoneByName($name = 'UTC')
    {
        $timezoneByName = self::getTimezones()[$name];

        return empty($timezoneByName) ? [] : $timezoneByName;
    }

    /**
     * Получить временные зоны по отклонения и направлению отклонения
     *
     * @param $offset
     * @param boolean $inverseOffset
     * @return array
     */
    public static function getTimezoneByOffset($offset = '00:00', $inverseOffset = false)
    {
        $result = [];

        $timezones = self::getTimezones();
        $offsetPrefix = $inverseOffset ? '-' : '+';
        foreach ($timezones as $timezone) {
            if ($timezone['offset'] == $offset && $timezone['offset_prefix'] == $offsetPrefix) {
                $result[$timezone['name']] = $timezone;
            }
        }

        return $result;
    }

    /**
     * Получить текущую дату/время с отклонением по временной зоне
     *
     * @param string $timezoneName
     * @param bool $timestamp формат выдачи по умолчанию текстовой вид
     * @param string $outputFormat Формат вывода значений
     * @return mixed
     */
    public static function getCurrentDateTimeWithTimezone(
        $timezoneName = 'UTC',
        $timestamp = false,
        $outputFormat = 'datetime'
    )
    {
        $currentDate = Carbon::now($timezoneName);

        return self::formatOutputDateTime($currentDate->timestamp, $timestamp, $outputFormat);
    }

    /**
     * Получить указанную дату/время с отклонением по временной зоне
     *
     * @param null $dateWithoutTimezone
     * @param string $timezoneName
     * @param bool $timestampInput формат приема даты по умолчанию время в секундах от 1970
     * @param bool $timestampOutput формат выдачи по умолчанию текстовой вид
     * @param string $outputFormat Формат вывода значений
     * @return mixed
     */
    public static function getDateTimeWithTimezone(
        $dateWithoutTimezone = null,
        $timezoneName = 'UTC',
        $timestampInput = true,
        $timestampOutput = false,
        $outputFormat = 'datetime'
    )
    {
        if (!$dateWithoutTimezone) {
            return self::getCurrentDateTimeWithTimezone($timezoneName, $timestampOutput, $outputFormat);
        }

        $timestampDate = null;
        if (!$timestampInput) {
            $timestampDate = strtotime($dateWithoutTimezone);
        } else {
            $timestampDate = $dateWithoutTimezone;
        }

        $date = Carbon::createFromTimestamp($timestampDate, $timezoneName);

        return self::formatOutputDateTime($date->timestamp, $timestampOutput, $outputFormat);
    }

    /**
     * Формат вывода даты/времени
     *
     * @param $timestamp
     * @param bool $timestampOutput
     * @param string $outputFormat
     * @return mixed
     */
    private static function formatOutputDateTime($timestamp, $timestampOutput = true, $outputFormat = '')
    {
        $date = Carbon::createFromTimestamp($timestamp);

        switch ($outputFormat) {
            case 'date':
                $formattedDate = $date->toDateString();
                $timestampDate = strtotime($formattedDate);
                break;
            case 'current_time':
                $formattedDate = $date->toTimeString();
                $timestampDate = strtotime($formattedDate);
                break;
            case 'time':
                $formattedDate = $date->toTimeString();
                $timestampDate = strtotime('1970-01-01' . $formattedDate);
                break;
            case 'datetime':
            default:
                $formattedDate = $date->toDateTimeString();
                $timestampDate = $date->timestamp;
                break;
        }

        return $timestampOutput ? $timestampDate : $formattedDate;
    }
}