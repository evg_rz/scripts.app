<?php
namespace App\Http\Controllers;

use App\Base\Result;
use App\Service\DateTimeService;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DateTimeController extends Controller
{
    public function getTimezoneList()
    {
        $result = new Result();

        $timezones = DateTimeService::getTimezones();

        $result->setIsValid(true);
        $result->setMessage($timezones);

        dd($timezones);

        return response()->json($result->asArray());
    }

    public function getTimezoneByName(Request $request)
    {
        $result = new Result();

        $validator = \Validator::make($request->all(), [
            'name' => 'required|string'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();

            $result->setCode(Result::INVALID_REQUEST_PARAMETERS);
            $result->setErrorMessage($messages);

            return response()->json($result->asArray());
        }

        $timezoneName = $request->get('name');

        $timezone = DateTimeService::getTimezoneByName($timezoneName);

        $result->setIsValid(true);
        $result->setMessage($timezone);

        dd($timezone);

        return response()->json($result->asArray());
    }

    public function getTimezoneByOffset(Request $request)
    {
        $result = new Result();

        $validator = \Validator::make($request->all(), [
            'offset' => 'required|string',
            'inverse' => 'boolean'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();

            $result->setCode(Result::INVALID_REQUEST_PARAMETERS);
            $result->setErrorMessage($messages);

            return response()->json($result->asArray());
        }

        $timezoneOffset = $request->get('offset');
        $timezoneInverseOffset = $request->get('inverse', false);

        $timezone = DateTimeService::getTimezoneByOffset($timezoneOffset, $timezoneInverseOffset);

        $result->setIsValid(true);
        $result->setMessage($timezone);

        dd($timezone);

        return response()->json($result->asArray());
    }

    public function getCurrentDateTimeWithTimezone(Request $request)
    {
        $result = new Result();

        $validator = \Validator::make($request->all(), [
            'timezone' => 'string',
            'timestamp' => 'boolean',
            'format' => Rule::in([ 'datetime', 'date', 'time', 'current_time' ])
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();

            $result->setCode(Result::INVALID_REQUEST_PARAMETERS);
            $result->setErrorMessage($messages);

            return response()->json($result->asArray());
        }

        $timezoneName = $request->get('timezone', 'UTC');
        $outputFormat = $request->get('format', 'datetime');
        $timestampOutput = $request->get('timestamp', false);

        $currentDateTime = DateTimeService::getCurrentDateTimeWithTimezone(
            $timezoneName,
            $timestampOutput,
            $outputFormat
        );

        $result->setIsValid(true);
        $result->setMessage($currentDateTime);

        dd($currentDateTime);

        return response()->json($result->asArray());
    }

    public function getDateTimeWithTimezone(Request $request)
    {
        $result = new Result();

        $validator = \Validator::make($request->all(), [
            'timezone' => 'string',
            'timestamp_input' => 'boolean',
            'timestamp_output'=> 'boolean',
            'format' => Rule::in([ 'datetime', 'date', 'time', 'current_time' ])
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();

            $result->setCode(Result::INVALID_REQUEST_PARAMETERS);
            $result->setErrorMessage($messages);

            return response()->json($result->asArray());
        }

        $timezoneName = $request->get('timezone', 'UTC');
        $dateWithoutTimezone = $request->get('date', null);
        $outputFormat = $request->get('format', 'datetime');
        $timestampInput = $request->get('timestamp_input', true);
        $timestampOutput = $request->get('timestamp_output', false);

        $dateTime = DateTimeService::getDateTimeWithTimezone(
            $dateWithoutTimezone,
            $timezoneName,
            $timestampInput,
            $timestampOutput,
            $outputFormat
        );

        $result->setIsValid(true);
        $result->setMessage($dateTime);

        dd($dateTime);

        return response()->json($result->asArray());
    }
}
